import subprocess

import config


class GitRepo(object):
    def __init__(self, directory):
        self.abs_directory = config.falcon_home + directory
        self.directory = directory

    def checkout(self, branch):
        try:
            branch = self.get_branch_with_text(branch)
            if branch is not None:
                output = subprocess.check_output(['git', 'checkout', branch], cwd=self.abs_directory)
                print (self.abs_directory)
                print (output)
        except subprocess.CalledProcessError as e:
            print('Exception checking out ' + self.directory + ' ' + e.message)

    def pull(self):
        return subprocess.call(['git', 'pull'], cwd=self.abs_directory)

    def merge(self, branch_to_merge_from):
        return subprocess.call(['git', 'merge', branch_to_merge_from], cwd=self.abs_directory)

    def status(self):
        try:
            output = subprocess.check_output(['git', 'status'], cwd=self.abs_directory)
            print (self.directory)
            print (output)
        except subprocess.CalledProcessError as e:
            print('Exception ' + self.directory)

    def branch(self):
        try:
            output = subprocess.check_output(['git', 'branch', '-a'], cwd=self.abs_directory)
            return output
        except subprocess.CalledProcessError as e:
            print('No branches for ' + self.directory)

    def branch_remote(self):
        try:
            subprocess.call(['git', 'fetch', '-p'])
            output = subprocess.check_output(['git', 'branch', '-a'], cwd=self.abs_directory)
            return output
        except subprocess.CalledProcessError as e:
            print('No branches for ' + self.directory)

    def get_branch_with_text(self, branch_to_merge_to):
        branch_output = self.branch()
        for branch in iter(branch_output.splitlines()):
            if branch_to_merge_to in branch:
                return branch.replace('*', '').replace('remotes/origin/', '').strip()

    def ls_remote(self):
        self.call_git(['ls-remote'])

    def call_git(self, args):
        subprocess.call(['git'] + args, cwd=self.abs_directory)

    def clean_remotes(self):
        self.call_git(['fetch', '-p'])

    def delete(self, branch_name):
        self.call_git(['branch', '-d', branch_name])

import argparse
import os
import sys
import config
import git_client
import utils

falcon_home = config.falcon_home


def get_falcon_git_repositories():
    return [name for name in utils.get_falcon_directories()
            if os.path.exists(falcon_home + name + '/.git')]


class GitSubCommands(object):
    def __init__(self):
        utils.init_commands(self,
                            description='Smart composite git commands',
                            command='git_command',
                            arg_range=sys.argv[2:3])

    def branches(self):
        for repo in get_falcon_git_repositories():
            output = git_client.GitRepo(repo).branch()
            print (repo)
            print (output)

    def all_branches(self):
        for repo in get_falcon_git_repositories():
            output = git_client.GitRepo(repo).branch_remote()
            print (repo)
            print (output)

    def has_branch(self):
        parser = argparse.ArgumentParser(
            description='Lists the repos that have a branch that contains the'
                        ' parameter in the name')
        parser.add_argument('part_of_branch_name')
        args = parser.parse_args(sys.argv[3:])
        for repo_dir in get_falcon_git_repositories():
            repo = git_client.GitRepo(repo_dir)
            branch_name = repo.get_branch_with_text(args.part_of_branch_name)

            if branch_name is not None:
                print('Repo ' + repo_dir + ' has branch ' + branch_name)

    def status(self):
        for repo_dir in get_falcon_git_repositories():
            git_client.GitRepo(repo_dir).status()

    def ls_remote(self):
        for repo_dir in get_falcon_git_repositories():
            git_client.GitRepo(repo_dir).ls_remote()

    def checkout(self):
        parser = argparse.ArgumentParser(
            description='Checks the specified branch')
        parser.add_argument('part_of_branch_name')
        args = parser.parse_args(sys.argv[3:])
        for repo_dir in get_falcon_git_repositories():
            repo = git_client.GitRepo(repo_dir)
            branch_name = repo.get_branch_with_text(args.part_of_branch_name)

            if branch_name is not None:
                repo.checkout(branch_name)

    def clean_remotes(self):
        for repo_dir in get_falcon_git_repositories():
            git_client.GitRepo(repo_dir).clean_remotes()

    def delete_branches(self):
        parser = argparse.ArgumentParser(
            description='Checks the specified branch')
        parser.add_argument('part_of_branch_name')
        args = parser.parse_args(sys.argv[3:])
        for repo_dir in get_falcon_git_repositories():
            repo = git_client.GitRepo(repo_dir)
            branch_name = repo.get_branch_with_text(args.part_of_branch_name)

            if branch_name is not None:
                print ('Trying to delete branch: ' + ' for repo ' + repo_dir)
                repo.checkout('develop')
                repo.delete(branch_name)


    def merge(self):
        parser = argparse.ArgumentParser(
            description='Merges branches into another branch on muliple repositories')
        parser.add_argument('branch_to_merge_from')
        parser.add_argument('branch_to_merge_to')
        args = parser.parse_args(sys.argv[3:])
        print(get_falcon_git_repositories())
        for directory in get_falcon_git_repositories():
            repo = git_client.GitRepo(directory)
            branch_to_merge_to_name = repo.get_branch_with_text(args.branch_to_merge_to)

            if branch_to_merge_to_name is not None:
                repo.checkout(args.branch_to_merge_from)
                repo.pull()
                repo.checkout(branch_to_merge_to_name)
                repo.pull()
                repo.merge(args.branch_to_merge_from)

    def sync_dev(self):
        parser = argparse.ArgumentParser(
            description='Merges branches into another branch on muliple repositories')
        print(get_falcon_git_repositories())
        for directory in get_falcon_git_repositories():
            repo = git_client.GitRepo(directory)
            print ('Syncing develop and master for ' + repo)
            repo.checkout('master')
            repo.pull()
            repo.merge('develop')
            repo.checkout('develop')
            repo.pull()
            repo.merge('master')

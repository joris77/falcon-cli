import argparse
import os
import config

def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]


def get_falcon_directories():
    return [name for name in get_immediate_subdirectories(config.falcon_home)
            if name.startswith('falcon-')]


def get_shell_scripts(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isfile(os.path.join(a_dir, name)) & name.endswith('.sh')]


def init_commands(self, description, command, arg_range, usage=None):
    parser = argparse.ArgumentParser(
        description=description, usage=usage)
    parser.add_argument(command)
    # parse_args defaults to [1:] for args, but you need to
    # exclude the rest of the args too, or validation will fail
    args = parser.parse_args(arg_range)
    if not hasattr(self, getattr(args, command)):
        print('Unrecognized git command')
        parser.print_help()
        exit(1)
    # use dispatch pattern to invoke method with same name
    getattr(self, getattr(args, command))()
# Prerequisite
* Python 2.7
* Git installed
* Docker installed
* Pip with docker-py
* Falcon workspaces

# Do
* Copy config.py.template to config.py
* Fill in the right values in the config.py
* Make symbolic from somewhere on your path to falcon
 
# Run
* falcon git branches
